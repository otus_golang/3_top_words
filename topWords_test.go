package topWords

import "testing"

func TestTopWords(t *testing.T) {
	text := `a s d q q w w e e r r t t y y u u i i o o p p z x c`
	get := topWords(text)
	wantWords := map[string]bool{
		"q": true,
		"w": true,
		"e": true,
		"r": true,
		"t": true,
		"y": true,
		"u": true,
		"i": true,
		"o": true,
		"p": true,
		"a": false,
		"s": false,
	}

	for word, isSet := range wantWords {
		_, ok := get[word]
		if ok != isSet {
			t.Errorf("Error with word: %s", word)
		}
	}
}
