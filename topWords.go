package topWords

import (
	"sort"
	"strings"
)

type wordUsage struct {
	Word  string
	Usage int
}

func topWords(text string) map[string]int {
	result := make(map[string]int)
	words := strings.Split(text, " ")

	wordFrequencies := make(map[string]int)

	for _, word := range words {
		wordFrequencies[word] = wordFrequencies[word] + 1
	}

	var rating []wordUsage
	for k, v := range wordFrequencies {
		rating = append(rating, wordUsage{k, v})
	}

	sort.Slice(rating, func(i, j int) bool {
		return rating[i].Usage > rating[j].Usage
	})

	for _, wortUsage := range rating[0:10] {
		result[wortUsage.Word] = wortUsage.Usage
	}

	return result
}
